# screenPushTilExit

This script will look for the running screen in a given SystemD service's CGroup and wait for it to exit. Optionally, this script can also send a string to the screen; a stop command for a server for example.

## Usage Instructions

Extract the tarball to /; the script will be installed at '/usr/local/bin/screenPushTilExit'.

* Pass the name of the screen (excluding the PID) as the first argument (for example, if the screen's name is '9999.minecraft', pass 'minecraft').
* Pass the SystemD service's unit name as the second argument ('minecraft.service' for example; the part after the '.' is required).
* Optionally, pass the stop string as the third argument; for a Minecraft server, we would pass "stop^M" to make the Minecraft server shut down. 

When the script is waiting for the screen to stop it will cycle between prining 1 '-' 9/10 seconds, and then print a 'x' on the 10th second before restarting the cycle. When the 'x' is printed, the stop string will be passed if it has been set. This continues as long as the screen exists. 

### Dependencies

Ensure the following commands are available:

* screen - The G.N.U. Screen utility. 

I used Bash version 4.4.12(1)-release but other relatively new versions should be fine as well. 
